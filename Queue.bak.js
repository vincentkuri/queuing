/* Implementing Inheritance from the following link :
 * http://www.crockford.com/javascript/inheritance.html
 * Also known as 'sugar' methods : method, inherits and swiss
*/

Function.prototype.method = function (name, func) {
    this.prototype[name] = func;
    return this;
};

Function.method('inherits', function (parent) {
    this.prototype = new parent();
    var d = {}, 
        p = this.prototype;
    this.prototype.constructor = parent; 
    this.method('uber', function uber(name) {
        if (!(name in d)) {
            d[name] = 0;
        }        
        var f, r, t = d[name], v = parent.prototype;
        if (t) {
            while (t) {
                v = v.constructor.prototype;
                t -= 1;
            }
            f = v[name];
        } else {
            f = p[name];
            if (f == this[name]) {
                f = v[name];
            }
        }
        d[name] += 1;
        r = f.apply(this, Array.prototype.slice.apply(arguments, [1]));
        d[name] -= 1;
        return r;
    });
    return this;
});

Function.method('swiss', function (parent) {
    for (var i = 1; i < arguments.length; i += 1) {
        var name = arguments[i];
        this.prototype[name] = parent.prototype[name];
    }
    return this;
});


// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


/* Queue class with the followig functions
 * getCount : returns the number of elements in the Queue
 * enqueue : function to add an element into the Queue
 * dequeue : function to remove the first element from the Queue (FIFO)
*/

function Queue(){
	this.array = [];
	this.count = 0;
}

Queue.method('enqueue', function(obj){
	this.count++;
	this.array.push(obj);
	return this;
});

Queue.method('dequeue', function(){
	if(this.count == 0)
		return null;
	--this.count;
	return this.array.shift();
});

Queue.method('getCount', function(){
	return this.count;
});


// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

/* TaskQueue class with the following functions
 * fire : fires the request
 * getNumOfPendingTasks : function to get the number of pending tasks in the Queue
 * getTimeEstimate : function to get the remaining time estimate
*/
var temp_obj;

function TaskQueue(){
	this.parallel_requests = 2;
	this.active_requests = 0;
	this.time_estimate = 0;
}

TaskQueue.inherits(Queue);

TaskQueue.method('fire', function(){
	var self = this;
	if(!this.getCount())
		return null;
	else{
		if(this.active_requests != this.parallel_requests){
			console.log("firing Request");
			this.active_requests++;
			temp_obj = this.dequeue();
			temp_obj.start_time = Date.now();

			if(!temp_obj)
				return null;
			if(temp_obj.type == "POST" || temp_obj.type == "PUT"){
				xhr = $.ajax({
					type:temp_obj.type,
					url:temp_obj.url,
					cache:false,
					data:temp_obj.data,
					context:temp_obj,
				}).done(function(){
					console.log("completed successfully");
					self.active_requests--;
					this.success();
					time_taken = (Date.now() - this.start_time)/1000;
					self.time_estimate = (self.time_estimate < time_taken)?time_taken:self.time_estimate;
					self.fire();
				}).fail(function(){
					console.log("failed");
					self.active_requests--;
					this.failure();
					time_taken = (Date.now() - this.start_time)/1000;
					self.time_estimate = (self.time_estimate < time_taken)?time_taken:self.time_estimate;
					self.fire();
				});
			}
			else if(temp_obj.type == "GET"){
				xhr = $.ajax({
					type:temp_obj.type,
					url:temp_obj.url,
					cache:false,
					context:temp_obj,
				}).done(function(){
					console.log("completed successfully");
					self.active_requests--;
					this.success();
					time_taken = (Date.now() - this.start_time)/1000;
					self.time_estimate = (self.time_estimate < time_taken)?time_taken:self.time_estimate;
					self.fire();
				})
				.fail(function(){
					console.log("failed");
					self.active_requests--;
					this.failure();
					time_taken = (Date.now() - this.start_time)/1000;
					self.time_estimate = (self.time_estimate < time_taken)?time_taken:self.time_estimate;
					self.fire();
				});
			}
			self.fire();
		}
	}
});

TaskQueue.method('getNumOfPendingTasks', function(){
	return this.getCount();
});

TaskQueue.method('getNumOfActiveTasks', function(){
	return this.active_requests;
});

TaskQueue.method('getPendingTime', function(){
	if(!this.time_estimate)
		return 0;
	else
		if(this.getNumOfPendingTasks() == 0)
			return this.time_estimate * this.getNumOfActiveTasks();
		else
			return this.time_estimate * (this.getNumOfPendingTasks() + this.getNumOfActiveTasks());
});